package com.nekitapp.koinsample.ui.main

import com.nekitapp.koinsample.data.db.entity.UserModel
import com.nekitapp.koinsample.ui.base.BaseVM
import kotlinx.coroutines.launch

interface MainVMContract {
    fun insertUser2DB(name: String, salary: Int)
}

class MainVM : BaseVM(), MainVMContract {

    override fun insertUser2DB(name: String, salary: Int) {
        launch {
            database.userDao().insertUser(UserModel(name = name, salary = salary))
        }
    }
}