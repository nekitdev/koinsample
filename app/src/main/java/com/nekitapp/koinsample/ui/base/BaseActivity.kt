package com.nekitapp.koinsample.ui.base

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel

abstract class BaseActivity : AppCompatActivity() {
    abstract val layoutRes: Int
    abstract val viewModel: ViewModel
}
