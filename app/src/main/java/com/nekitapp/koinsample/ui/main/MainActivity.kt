package com.nekitapp.koinsample.ui.main

import android.os.Bundle
import com.nekitapp.koinsample.R
import com.nekitapp.koinsample.ui.base.BaseActivity
import org.koin.android.scope.currentScope
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    override val layoutRes = R.layout.activity_main
    /**Scope*/
//    override val viewModel: MainVM by currentScope.inject()
    /**ViewModel*/
    override val viewModel: MainVM by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.insertUser2DB("Name", 1_000_000)
    }
}
