package com.nekitapp.koinsample.ui.base

import android.util.Log
import androidx.lifecycle.ViewModel
import com.nekitapp.koinsample.data.db.DBManager
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject


open class BaseVM : ViewModel(), CoroutineScope, KoinComponent {

    val database: DBManager by inject()

    override val coroutineContext =
        SupervisorJob() + Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            baseExceptionHandler(throwable)
        }

    private fun baseExceptionHandler(throwable: Throwable) {
        Log.e("Error", throwable.message, throwable)
    }

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancel()
    }
}