package com.nekitapp.koinsample.core

import android.app.Application
import com.facebook.stetho.Stetho
import com.nekitapp.koinsample.data.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class KoinApp : Application() {

    override fun onCreate() {
        super.onCreate()

        /** Start koin*/
        startKoin {
            androidContext(this@KoinApp)
            modules(listOf(appModule))
        }

        /**Stetho*/
        Stetho.initializeWithDefaults(this)

    }
}