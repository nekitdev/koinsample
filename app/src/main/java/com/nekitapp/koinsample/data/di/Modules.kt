package com.nekitapp.koinsample.data.di

import androidx.room.Room
import com.nekitapp.koinsample.data.db.DBManager
import com.nekitapp.koinsample.ui.main.MainActivity
import com.nekitapp.koinsample.ui.main.MainVM
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    /**Single*/
    single {
        Room.databaseBuilder(androidContext(), DBManager::class.java, "sample_base")
            .build()
    }
//    single { MainVM() }

    /**Scope*/
    scope(named<MainActivity>()) {
        scoped {
            MainVM()
        }
    }

    /**ViewModel*/
    viewModel { MainVM() }
}

