package com.nekitapp.koinsample.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nekitapp.koinsample.data.db.dao.UserDao
import com.nekitapp.koinsample.data.db.entity.UserModel

@Database(entities = [(UserModel::class)], version = 1)

abstract class DBManager : RoomDatabase() {

    abstract fun userDao(): UserDao

}
