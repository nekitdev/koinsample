package com.nekitapp.koinsample.data.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.nekitapp.koinsample.data.db.entity.UserModel

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getAllUsers(): List<UserModel>

    @Insert
    fun insertUser(userModel: UserModel)

    @Delete
    fun deleteUser(userModel: UserModel)

}