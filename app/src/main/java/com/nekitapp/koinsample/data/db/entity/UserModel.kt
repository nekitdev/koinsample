package com.nekitapp.koinsample.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "User")
class UserModel(
    @PrimaryKey(autoGenerate = true) val id: Long? = null,
    var name: String,
    var salary: Int
)